package com.monsato.magazine.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.monsato.magazine.model.Customer;
import com.monsato.magazine.repository.CustomerRepository;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.*;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class CustomerControllerTest {

    @Autowired
    private TestRestTemplate template;

    @Autowired
    private CustomerRepository customerRepository;

    private ResponseEntity<Customer> resultAsset;

    private ObjectMapper objectMapper = new ObjectMapper();

    @Before
    public void beforeEach() throws Exception {
        Customer customer = new Customer();
        customer.setEmail("testEmail@mansato.com");
        customer.setName("Rainer Lyt");
        HttpEntity<Object> customerJson = getHttpEntity(objectMapper.writeValueAsString(customer));
        resultAsset = template.postForEntity("/customers", customerJson, Customer.class);
        assertNotNull(resultAsset.getBody());
        assertNotNull(resultAsset.getBody().getId());
    }

    @After
    public void afterEach() throws Exception {
        customerRepository.deleteById(Objects.requireNonNull(resultAsset.getBody()).getId());
    }

    @Test
    public void testCustomerShouldBeCreated() {
        assertEquals(resultAsset.getStatusCode(), HttpStatus.CREATED);
    }

    @Test
    public void testCustomersShouldBeReturned() {
        ResponseEntity<Customer[]> responseEntity = template.getForEntity("/customers", Customer[].class);
        assertNotNull(responseEntity.getBody());
        assertEquals(responseEntity.getStatusCode(), HttpStatus.OK);
        List<Customer> customers = Arrays.asList(Objects.requireNonNull(responseEntity.getBody()));
        assertEquals(customers, customerRepository.findAll());
    }

    private HttpEntity<Object> getHttpEntity(Object body) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        return new HttpEntity<>(body, headers);
    }
}