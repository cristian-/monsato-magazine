package com.monsato.magazine.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.monsato.magazine.dto.CustomerAndOrdersDTO;
import com.monsato.magazine.model.Customer;
import com.monsato.magazine.model.Order;
import com.monsato.magazine.repository.CustomerRepository;
import com.monsato.magazine.repository.OrderRepository;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.*;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment =  WebEnvironment.RANDOM_PORT)
public class OrderControllerTest {

    @Autowired
    private TestRestTemplate template;

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private OrderRepository orderRepository;

    private ObjectMapper objectMapper = new ObjectMapper();
    private ResponseEntity<Customer> savedCustomer;

    @Before
    public void beforeEach() throws Exception {
        Customer customer = new Customer();
        customer.setEmail("user1@gmail.com");
        customer.setName("Rainer Lyt");
        HttpEntity<Customer> customerHttpEntity = getHttpEntity(customer);
        savedCustomer = template.postForEntity("/customers", customerHttpEntity, Customer.class);
        assertNotNull(savedCustomer.getBody());
        assertNotNull(savedCustomer.getBody().getId());
    }

    @After
    public void afterEach() throws Exception {
        customerRepository.deleteById(Objects.requireNonNull(savedCustomer.getBody()).getId());
    }

    @Test
    public void orderShouldBeCreated() {
        ResponseEntity<Order> orderResponseEntity = saveOrder();
        assertEquals(orderResponseEntity.getStatusCode(), HttpStatus.CREATED);
    }


    @Test
    public void shouldBeFoundAllOrdersForCustomer() {
        saveOrder();
        ResponseEntity<Order[]> ordersResponseEntity = template.getForEntity(String.format("/customers/%s/orders", Objects.requireNonNull(savedCustomer.getBody()).getId()), Order[].class);
        assertEquals(ordersResponseEntity.getStatusCode(), HttpStatus.OK);
        List<Order> orders = Arrays.asList(Objects.requireNonNull(ordersResponseEntity.getBody()));
        assertEquals(orders, orderRepository.findByCustomerId(savedCustomer.getBody().getId()));
    }

    @Test
    public void shouldBeFoundAllOrdersAndItsCustomer() {
        saveOrder();
        ResponseEntity<CustomerAndOrdersDTO> ordersResponseEntity = template.getForEntity(String.format("/customers/%s/customer-and-orders", Objects.requireNonNull(savedCustomer.getBody()).getId()), CustomerAndOrdersDTO.class);
        assertEquals(ordersResponseEntity.getStatusCode(), HttpStatus.OK);
        CustomerAndOrdersDTO responseCustomerAndOrdersDTO = ordersResponseEntity.getBody();
        CustomerAndOrdersDTO customerAndOrdersDTO = new CustomerAndOrdersDTO();
        customerAndOrdersDTO.setCustomer(savedCustomer.getBody());
        customerAndOrdersDTO.setOrders(orderRepository.findByCustomerId(savedCustomer.getBody().getId()));
        assertEquals(responseCustomerAndOrdersDTO, customerAndOrdersDTO);
    }

    private ResponseEntity<Order> saveOrder() {
        Order order = new Order();
        order.setCreateDate(LocalDateTime.now());
        order.setPrice(200.00);

        HttpEntity<Order> httpEntity = getHttpEntity(order);
        return template.postForEntity(String.format("/customers/%s/orders", Objects.requireNonNull(savedCustomer.getBody()).getId()), httpEntity, Order.class);
    }

    private <T> HttpEntity<T> getHttpEntity(T body) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        return new HttpEntity<>(body, headers);
    }
}