package com.monsato.magazine.dto;

import com.monsato.magazine.model.Customer;
import com.monsato.magazine.model.Order;

import java.util.List;
import java.util.Objects;

//Data Transfer Object for returning Customer and his Orders
public class CustomerAndOrdersDTO {
    Customer customer;
    List<Order> orders;

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public List<Order> getOrders() {
        return orders;
    }

    public void setOrders(List<Order> orders) {
        this.orders = orders;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CustomerAndOrdersDTO that = (CustomerAndOrdersDTO) o;
        return Objects.equals(customer, that.customer) &&
                Objects.equals(orders, that.orders);
    }

    @Override
    public int hashCode() {
        return Objects.hash(customer, orders);
    }
}
