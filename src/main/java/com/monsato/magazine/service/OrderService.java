package com.monsato.magazine.service;


import com.monsato.magazine.model.Order;
import java.util.List;

public interface OrderService {
    /*
     * Save order.
     */
    Order save(Order order);

    /*
     * Returns all the Orders related to Customers.
     */
    List<Order> findByCustomer(Long customerId);

}
