package com.monsato.magazine.service;

import com.monsato.magazine.model.Customer;

import java.util.List;

/*
 * This interface provides all methods to access the functionality. See CustomerServiceImpl for implementation.
 *
 * @author ilie.cristian
 */
public interface CustomerService {
    /*
     * Save customer.
     */
    Customer save(Customer customer);

    /*
     * Return all customers.
     */
    List<Customer> findAll();

    /*
     * Return customer by ID.
     *
     */
    Customer findById(Long id);

}
