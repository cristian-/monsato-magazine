package com.monsato.magazine.repository;

import com.monsato.magazine.model.Customer;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import java.util.List;

@RepositoryRestResource(exported=false)
public interface CustomerRepository extends PagingAndSortingRepository<Customer,Long> {

    List<Customer> findAll();

}
