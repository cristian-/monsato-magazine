package com.monsato.magazine.controller;

import com.monsato.magazine.dto.CustomerAndOrdersDTO;
import com.monsato.magazine.model.Order;
import com.monsato.magazine.service.CustomerService;
import com.monsato.magazine.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class OrderController {

    private final OrderService orderService;

    private final CustomerService customerService;

    @Autowired
    public OrderController(OrderService orderService, CustomerService customerService) {
        this.orderService = orderService;
        this.customerService = customerService;
    }

    @PostMapping(path = "customers/{customer-id}/orders")
    public ResponseEntity<Order> createOrder(@PathVariable(value = "customer-id") Long customerId, @RequestBody Order order) {
        order.setCustomer(customerService.findById(customerId));
        return new ResponseEntity<>(orderService.save(order), HttpStatus.CREATED);
    }

    @GetMapping(path = "customers/{customer-id}/orders")
    public ResponseEntity<List<Order>> getOrders(@PathVariable("customer-id") Long customerId) {
        return new ResponseEntity<>(orderService.findByCustomer(customerId), HttpStatus.OK);
    }

    @GetMapping(path = "customers/{customer-id}/customer-and-orders")
    public ResponseEntity<CustomerAndOrdersDTO> getCustomerAndOrders(@PathVariable("customer-id") Long customerId) {
        CustomerAndOrdersDTO customerAndOrdersDTO = new CustomerAndOrdersDTO();
        customerAndOrdersDTO.setCustomer(customerService.findById(customerId));
        customerAndOrdersDTO.setOrders(orderService.findByCustomer(customerId));
        return new ResponseEntity<>(customerAndOrdersDTO, HttpStatus.OK);
    }

}
